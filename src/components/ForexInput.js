import React, { Component } from 'react';
import { FormGroup, Label, Input } from 'reactstrap';

class ForexInput extends Component {
    state = {  }
    render() { 
        return ( 
            <FormGroup>
                <Label>
                    <Input
                        label = {this.props.label}
                        placeholder = { this.props.placeholder}
                        defaultValue = {this.props.defaultValue}
                        onChange = {this.props.onChange}
                        type = 'number'
                    />
                </Label>
            </FormGroup>
         );
    }
}
 
export default ForexInput;