import React, { Component } from 'react';
import Box from './components/Box';
import Buttons from './components/Buttons';

class App extends Component{
  state = {
    count: 0,
  }

  handleAdd = () => {
    this.setState({
      count: this.state.count + 1
    })
  };

  handleMinus = () => {
    this.setState({
      count: this.state.count - 1
    })
  };

  handleReset = () => {
    this.setState({
      count: 0
    })
  };

  handleTimesTwo = () => {
    this.setState({
      count: this.state.count *2
    })
  };
  

  render() {
    return (
      <React.Fragment>
        <Box
          count = {this.state.count}
        />  
        <Buttons 
          handleAdd = { this.handleAdd}
          handleMinus = { this.handleMinus}
          handleReset = { this.handleReset}
          handleTimesTwo = { this.handleTimesTwo}
        />
      </React.Fragment>
    )
  }
}
export default App;