import React, { Component } from 'react';
import Forex from './components/Forex'

class ForexApp extends Component {
    state = {  }
    render() { 
        return ( 
            <div
                className='bg-secondary d-flex justify-content-center align-items-center vh-100'
                >
                    <Forex />
            </div>
         );
    }
}
 
export default ForexApp;